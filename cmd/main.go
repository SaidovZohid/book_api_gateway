package main

import (
	"book/book_api_gateway/client"
	"book/book_api_gateway/genproto/book_service"
	"context"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Book struct {
	Title       string `json:"title"`
	Author      string `json:"author"`
	Description string `json:"description"`
	Language    string `json:"language"`
}

type GetBook struct {
	Id          int64  `json:"id"`
	Title       string `json:"title"`
	Author      string `json:"author"`
	Description string `json:"description"`
	Language    string `json:"language"`
}

type RouteOptions struct {
	grpcConn client.GrpcClientI
}

func (r *RouteOptions) Create(ctx *gin.Context) {
	var req Book
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	book, err := r.grpcConn.BookService().Create(context.Background(), &book_service.Book{
		Title:       req.Title,
		Author:      req.Author,
		Description: req.Description,
		Language:    req.Language,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusCreated, GetBook{
		Id:          book.Id,
		Title:       book.Title,
		Author:      book.Author,
		Description: book.Description,
		Language:    book.Language,
	})
}

func (r *RouteOptions) Get(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	book, err := r.grpcConn.BookService().Get(context.Background(), &book_service.GetBookRequest{
		Id: id,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}

	ctx.JSON(http.StatusOK, GetBook{
		Id:          book.Id,
		Title:       book.Title,
		Author:      book.Author,
		Description: book.Description,
		Language:    book.Language,
	})
}

func New(grpcConn client.GrpcClientI) *gin.Engine {
	router := gin.Default()
	r := RouteOptions{
		grpcConn: grpcConn,
	}
	router.POST("/books", r.Create)
	router.GET("/books/:id", r.Get)

	return router
}

func main() {
	grpcConn, err := client.New()

	if err != nil {
		log.Fatalf("failed to get grpc connettion: %v", err)
	}

	apiServer := New(grpcConn)

	err = apiServer.Run(":8080")
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}
}