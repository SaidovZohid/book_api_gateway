package client

import (
	pb "book/book_api_gateway/genproto/book_service"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	BookService() pb.BookServiceClient
}

type GrpcClient struct {
	connections map[string]interface{}
}

func New() (GrpcClientI, error) {
	connBookService, err := grpc.Dial("book_service:5000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("book service dial host: book_service port: 5000")
	}

	return &GrpcClient{
		connections: map[string]interface{}{
			"book_service": pb.NewBookServiceClient(connBookService),
		},
	}, nil
}

func (g *GrpcClient) BookService() pb.BookServiceClient {
	return g.connections["book_service"].(pb.BookServiceClient)
}