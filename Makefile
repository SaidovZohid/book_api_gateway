CURRENT_DIR=$(shell pwd)

proto-gen:
	rm -rf genproto
	./scripts/gen-proto.sh ${CURRENT_DIR}

run:
	go run cmd/main.go