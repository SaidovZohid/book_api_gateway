FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /book

COPY . .


RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /book

COPY --from=builder /book/main .

EXPOSE 8080

CMD [ "/book/main" ]